# MLOps3.0

## Description
This project aims to create service able to translate science papers using nllb model


## Overview
*under development*


## Repo Methodology

In this repository, we follow a specific methodology to ensure code quality and effective collaboration. Below are the main principles we're trying to follow:

### Branching

We use the Git Flow branching strategy. The main `main` branch contains the stable version of the code, ready for deployment. New features are developed in separate `feature/*` branches, and bug fixes are made in `hotfix/*` branches. After completing work on a feature or fix, it is merged into the `main` branch.

### Commits

We welcome these statements:

- Commit messages should describe the changes made.
- Each commit should be atomic and introduce logically related changes.

### Merge Requests

All changes to the `main` branch are made through Merge Requests (MRs). This allows for code review and discussion of the proposed changes before merging. MRs should include:

- A brief description of the changes made.
- References to related tasks or issues.
- Test results (if applicable).

### Continuous Integration (CI)

This project has a CI system set up, which automatically runs linting, tests, and builds on every Merge Request. This helps catch issues early and ensures code quality.

### Code Review

All Merge Requests go through a code review process, where other project members review the code for corresponding to standards, best practices, and architectural principles. This helps improve code quality and encourages knowledge sharing.